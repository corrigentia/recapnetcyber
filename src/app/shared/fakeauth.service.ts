import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IUser } from '../models/IUser';

@Injectable({
  providedIn: 'root'
})
export class FakeauthService {

  private _users : IUser[] = [
    { id : 1, firstname : 'Aude', login : 'audeb', password : 'Test1234'},
    { id : 2, firstname : 'Khun', login : 'khunl', password : 'Test5678'}
  ];

  // connectedUser : IUser | undefined;

  //Subject -> Valeur change lors d'un event
  //Behavior -> Déjà initialisé avec une valeur, se déclenche lors d'un event + au subscribe
  //private _$connectedUser : Subject<IUser | undefined> = new Subject<IUser | undefined>();
  private _$connectedUser : BehaviorSubject<IUser | undefined> = new BehaviorSubject<IUser | undefined>(undefined);

  //Transforme votre Subject/Behavior en Observable pour que le componant qui l'utilise ne puisse pas faire .next (.error, .complete) dessus, mais juste s'abonner
  $connectedUser : Observable<IUser | undefined> = this._$connectedUser.asObservable();

  constructor() {
   }

  login(login : string, mdp : string) /* : IUser | undefined */ : void {
    //On cherche le user dont le login est = au login reçu en param
    //Si pas trouvé, find renvoie undefined
    let user : IUser | undefined = this._users.find(u => u.login === login);
    if(user){
      //Si user pas undefined, on teste le mdp
      if(user.password === mdp) {
        //Si mdp ok, on renvoie le user
        //this.connectedUser = user;
        this._$connectedUser.next(user);
        //return user;
      }
      else {
        this._$connectedUser.next(undefined);
      }
    }
    else {
      this._$connectedUser.next(undefined);
    }
    //Si user pas trouvé grâce au login ou si mdp pas ok, on renvoie undefined
    //return undefined;
  }

  logout() : /* undefined */ void{
    //this.connectedUser = undefined;
    //En vrai : Retirer le token, faire le ménage
    //return undefined;
    this._$connectedUser.next(undefined);

  }
}
