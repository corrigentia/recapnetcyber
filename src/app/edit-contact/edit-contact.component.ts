import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit{

  monForm : FormGroup;

  constructor(private _activeRoute : ActivatedRoute, private _route : Router, private _fb : FormBuilder) {
   this.monForm = this._fb.group({
    lastname : [null, []],
    firstname : [null, []],
   })
  }

  ngOnInit(): void {
      console.log( this._activeRoute.snapshot.params['id']);
      let id : number = parseInt(this._activeRoute.snapshot.params['id']);
      
      // this._contactService.getById(id).subscribe((res) => {
      //   this.monContact = res;
      //   this.monForm.patchValue({
      //     lastname : this.monContact.lastname,
      //     firstname : this.monContact.firstname
      //   })
      //   this.monForm.updateValueAndValidity()
      // })

      
  }

  changePage() : void {
    this._route.navigateByUrl('/demo');
  }

}
